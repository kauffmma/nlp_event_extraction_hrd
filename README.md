# NLP_event_extraction_HRD

## Aims
Event extraction NLP pipeline, with focus on human right defenders.

## License
Code released under GNU Affero GPL 3 or later.
To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Contributor(s)
Mayeul Kauffmann
